package org.hobby.bpjstk.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.text.WordUtils;

@Data
@Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BpjsTkKpj {
    private String kodeTk;
    private String program;
    private String kpj;
    private String kodeSegmen;
    private String nomorIdentitas;
    private String namaTk;
    private String tglKepesertaan;

    public void setNomorIdentitas(String nomorIdentitas) {
        this.nomorIdentitas = nomorIdentitas.substring(0, nomorIdentitas.length() - 3) + "xxx";
    }

    public void setNamaTk(String namaTk) {
        this.namaTk = WordUtils.capitalizeFully(namaTk);
    }
}
