package org.hobby.bpjstk.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BpjsTkSalaryDto {
    private String kodePerusahaan;
    private String blth;
    private String kodeSegmen;
    private String kodeKepesertaan;
    private String kodeTk;
    private String kodeIuran;
    private String tglPniPelunasan;
    private double nomIuranJht;
    private double nomUpah;
}
