package org.hobby.bpjstk.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.commons.text.WordUtils;

import java.util.List;

@Data
@Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BpjsTkGeneral {
    private String nik;
    private String namaLengkap;
    private String nomorHp;
    private String email;
    private String alamat;
    private String tglLahir;
    private String jenisKelamin;
    private List<BpjsTkKpj> kpj;

    public void setNik(String nik) {
        this.nik = nik.substring(0, nik.length() - 3) + "xxx";
    }

    public void setNomorHp(String nomorHp) {
        this.nomorHp = nomorHp.substring(0, nomorHp.length() - 3) + "xxx";
    }

    public void setNamaLengkap(String namaLengkap) {
        this.namaLengkap = WordUtils.capitalizeFully(namaLengkap);
    }
}
