package org.hobby.bpjstk.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BpjsTkEmploymentDto {
    private String namaPerusahaan;
    private String statusKeps;
    private String tglKepsJp;
    private String tglPensiunJp;
    private String tglPniItrf;
    private int masaIurJp;
    private double nomUpahItrf;
}
