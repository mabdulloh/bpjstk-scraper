package org.hobby.bpjstk.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.AsyncHttpClientConfig;
import org.asynchttpclient.DefaultAsyncHttpClient;
import org.asynchttpclient.DefaultAsyncHttpClientConfig;
import org.asynchttpclient.proxy.ProxyServer;
import org.hobby.bpjstk.exception.ResourceInvalidException;
import org.hobby.bpjstk.model.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Slf4j
@Service
public class BpjsTkClientImpl implements BpjsTkClient {

    private static final AsyncHttpClient CLIENT = new DefaultAsyncHttpClient();
    private static final String BASE_URL = "https://api-mobile.bpjsketenagakerjaan.go.id/android/api/";
    private static final ObjectMapper MAPPER = new ObjectMapper();
    private static final int SUCCESS_CODE = 0;
    private static final String USER_AGENT = "okhttp/3.11.0";
    private static final String X_REGISTER_ID = "X-Register-Id";
    private static final String KEEP_ALIVE = "keep-alive";
    private static final String AUTHENTICATION = "Authentication";
    private final String randomString;
    private final AsyncHttpClient asyncHttpClient;

    public BpjsTkClientImpl() {
        randomString = generateString();
        asyncHttpClient = generateAsyncHttpClient();
    }

    public List<BpjsTkSalary> getSalaryInfo(String email, String password) {
        List<BpjsTkSalary> result = new ArrayList<>();
        var bpjsTkLogin = login(email, password);
        List<BpjsTkSegment> segments = getSegments(bpjsTkLogin.getToken());
        String salaryUrl = BASE_URL + "O3LzMrK6Osipz5apKNLT";
        try {
            for (BpjsTkSegment segment : segments) {
                String body = "kodePerusahaan=" + segment.getKodePerusahaan() +
                        "&kodeTK=" + segment.getKodeTk() +
                        "&kodeSegmen=" + segment.getKodeSegmen();
                var apiResponse = asyncHttpClient.preparePost(salaryUrl)
                        .addHeader(HttpHeaders.USER_AGENT, USER_AGENT)
                        .addHeader(HttpHeaders.HOST, BASE_URL)
                        .addHeader(HttpHeaders.CONNECTION, KEEP_ALIVE)
                        .addHeader(HttpHeaders.ACCEPT_ENCODING, "gzip")
                        .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                        .addHeader(X_REGISTER_ID, randomString)
                        .addHeader(AUTHENTICATION, bpjsTkLogin.getToken())
                        .setBody(body)
                        .execute().get();
                if (apiResponse.getStatusCode() == HttpStatus.OK.value()) {
                    JsonNode response = deserialize(apiResponse.getResponseBody(), JsonNode.class);
                    if (response.get("ret").asInt() == SUCCESS_CODE) {
                        result.addAll(Arrays.asList(deserialize(response.get("detilSaldo").toString(), BpjsTkSalary[].class)));
                    }
                }
            }
            logout(bpjsTkLogin.getToken());
            return result;
        } catch (InterruptedException | ExecutionException e) {
            log.warn(e.getMessage());
            Thread.currentThread().interrupt();
            throw new ResourceInvalidException(e.getMessage());
        }
    }

    public List<BpjsTkEmployment> getEmploymentInfo(String email, String password) {
        String employmentUrl = BASE_URL + "GccHgXEhjRpwYjXMjZQ5";
        var bpjsTkLogin = login(email, password);
        List<BpjsTkEmployment> result = new ArrayList<>();
        try {
            for (BpjsTkKpj item : bpjsTkLogin.getData().getKpj()) {
                String body = "kpj=" + item.getKpj() +
                        "&kodeTk=" + item.getKodeTk() +
                        "&kodeSegmen=" + item.getKodeSegmen();
                var apiResponse = asyncHttpClient.preparePost(employmentUrl)
                        .addHeader(HttpHeaders.USER_AGENT, USER_AGENT)
                        .addHeader(HttpHeaders.HOST, BASE_URL)
                        .addHeader(HttpHeaders.CONNECTION, KEEP_ALIVE)
                        .addHeader(HttpHeaders.ACCEPT_ENCODING, "gzip")
                        .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                        .addHeader(X_REGISTER_ID, randomString)
                        .addHeader(AUTHENTICATION, bpjsTkLogin.getToken())
                        .setBody(body)
                        .execute().get();
                if (apiResponse.getStatusCode() == HttpStatus.OK.value()) {
                    JsonNode response = deserialize(apiResponse.getResponseBody(), JsonNode.class);
                    if (response.get("ret").asInt() == SUCCESS_CODE) {
                        result.addAll(Arrays.asList(deserialize(response.get("detilKepsPU").toString(), BpjsTkEmployment[].class)));
                    }
                }
            }
            logout(bpjsTkLogin.getToken());
            return result;
        } catch (InterruptedException | ExecutionException e) {
            log.warn(e.getMessage());
            Thread.currentThread().interrupt();
            throw new ResourceInvalidException(e.getMessage());
        }
    }

    public BpjsTkGeneral getGeneralInfo(String email, String password) {
        var bpjsTkLogin = login(email, password);
        logout(bpjsTkLogin.getToken());
        return bpjsTkLogin.getData();
    }

    public List<BpjsTkSegment> getSegmentInfo(String email, String password) {
        var bpjsTkLogin = login(email, password);
        List<BpjsTkSegment> bpjsTkSegments = getSegments(bpjsTkLogin.getToken());
        logout(bpjsTkLogin.getToken());
        return bpjsTkSegments;
    }

    private BpjsTkLogin login(String email, String password) {
        String generalUrl = BASE_URL + "mt4GXToN4FUkbM41rhR5";
        email = URLEncoder.encode(email, StandardCharsets.UTF_8);
        password = URLEncoder.encode(password, StandardCharsets.UTF_8);
        String body = "email=" + email + "&kata_sandi=" + password;
        try {
            var response = asyncHttpClient.preparePost(generalUrl)
                    .setHeader(HttpHeaders.ACCEPT_ENCODING, "gzip")
                    .setHeader(HttpHeaders.USER_AGENT, USER_AGENT)
                    .setHeader(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded")
                    .setHeader(HttpHeaders.HOST, "api-mobile.bpjsketenagakerjaan.go.id")
                    .setHeader(HttpHeaders.CONNECTION, KEEP_ALIVE)
                    .setHeader(X_REGISTER_ID, randomString)
                    .setBody(body)
                    .execute().get();
            if (response.getStatusCode() == HttpStatus.OK.value()) {
                BpjsTkLogin result = deserialize(response.getResponseBody(), BpjsTkLogin.class);
                if (result.getRet() == SUCCESS_CODE) {
                    return result;
                } else {
                    throw new ResourceInvalidException("Data is not available");
                }
            } else {
                throw new ResourceInvalidException("Service unavailable");
            }
        } catch (ResourceInvalidException | InterruptedException | ExecutionException e) {
            log.warn(e.getMessage());
            Thread.currentThread().interrupt();
            throw new ResourceInvalidException(e.getMessage());
        }
    }

    private void logout(String token) {
        if (token != null) {
            String logoutUrl = BASE_URL + "zFPu9DG5W9d0uUKXAjOQ";
            try {
                asyncHttpClient.preparePost(logoutUrl)
                        .addHeader(HttpHeaders.USER_AGENT, USER_AGENT)
                        .addHeader(HttpHeaders.HOST, BASE_URL)
                        .addHeader(HttpHeaders.CONNECTION, KEEP_ALIVE)
                        .addHeader(HttpHeaders.ACCEPT_ENCODING, "gzip")
                        .addHeader(X_REGISTER_ID, randomString)
                        .addHeader(AUTHENTICATION, token)
                        .execute().get();
                log.info("Success logout");
            } catch (InterruptedException | ExecutionException e) {
                log.warn(e.getMessage());
                Thread.currentThread().interrupt();
                throw new ResourceInvalidException(e.getMessage());
            }
        } else {
            throw new ResourceInvalidException();
        }
    }

    private List<BpjsTkSegment> getSegments(String token) {
        String segmentUrl = BASE_URL + "LnFWV5PKU6NV6pWvlDT0";
        List<BpjsTkSegment> bpjsTkSegments = new ArrayList<>();
        try {
            var apiResponse = asyncHttpClient.preparePost(segmentUrl)
                    .addHeader(HttpHeaders.USER_AGENT, USER_AGENT)
                    .addHeader(HttpHeaders.HOST, BASE_URL)
                    .addHeader(HttpHeaders.CONNECTION, KEEP_ALIVE)
                    .addHeader(HttpHeaders.ACCEPT_ENCODING, "gzip")
                    .addHeader(X_REGISTER_ID, randomString)
                    .addHeader(AUTHENTICATION, token)
                    .execute().get();
            if (apiResponse.getStatusCode() == HttpStatus.OK.value()) {
                JsonNode response = deserialize(apiResponse.getResponseBody(), JsonNode.class);
                if (response.get("ret").asInt() == SUCCESS_CODE) {
                    bpjsTkSegments.addAll(Arrays.asList(deserialize(response.get("dataPU").toString(), BpjsTkSegment[].class)));
                } else {
                    throw new ResourceInvalidException(response.get("msg").toString());
                }
            }
            return bpjsTkSegments;
        } catch (InterruptedException | ExecutionException e) {
            log.warn(e.getMessage());
            Thread.currentThread().interrupt();
            throw new ResourceInvalidException(e.getMessage());
        }
    }

    private static <T> T deserialize(String value, Class<T> clazz) {
        try {
            return MAPPER.readValue(value, clazz);
        } catch (JsonProcessingException e) {
            throw new ResourceInvalidException(e.getMessage());
        }
    }

    private static String generateString() {
        return RandomStringUtils.randomAlphanumeric(30);
    }

    private static AsyncHttpClient generateAsyncHttpClient() {
        AsyncHttpClientConfig cf = new DefaultAsyncHttpClientConfig.Builder()
                .setProxyServer(new ProxyServer.Builder("222.165.235.254", 80)).build();
        return new DefaultAsyncHttpClient(cf);
    }
}
