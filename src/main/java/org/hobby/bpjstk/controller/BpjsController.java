package org.hobby.bpjstk.controller;

import org.hobby.bpjstk.client.BpjsTkClient;
import org.hobby.bpjstk.dto.*;
import org.hobby.bpjstk.model.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/bpjs")
public class BpjsController {

    @Autowired
    private BpjsTkClient bpjsTkClient;
    private static final ModelMapper MODEL_MAPPER = new ModelMapper();

    @PostMapping(value = "/general", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BpjsTkGeneralDto> getGeneralInfo(@RequestBody CredentialDto credentialDto) {
        BpjsTkGeneral result = bpjsTkClient.getGeneralInfo(credentialDto.getEmail(), credentialDto.getPassword());
        if (result != null) {
            return ResponseEntity.ok(MODEL_MAPPER.map(result, BpjsTkGeneralDto.class));
        }
        return ResponseEntity.unprocessableEntity().build();
    }

    @PostMapping(value = "/employment", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BpjsTkEmploymentDto[]> getEmploymentInfo(@RequestBody CredentialDto credentialDto) {
        List<BpjsTkEmployment> result = bpjsTkClient.getEmploymentInfo(credentialDto.getEmail(), credentialDto.getPassword());
        if (result != null) {
            return ResponseEntity.ok(MODEL_MAPPER.map(result, BpjsTkEmploymentDto[].class));
        }
        return ResponseEntity.unprocessableEntity().build();
    }

    @PostMapping(value = "/segment", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BpjsTkSegmentDto[]> getSegmentInfo(@RequestBody CredentialDto credentialDto) {
        List<BpjsTkSegment> result = bpjsTkClient.getSegmentInfo(credentialDto.getEmail(), credentialDto.getPassword());
        if (result != null) {
            return ResponseEntity.ok(MODEL_MAPPER.map(result, BpjsTkSegmentDto[].class));
        }
        return ResponseEntity.unprocessableEntity().build();
    }

    @PostMapping(value = "/salary", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BpjsTkSalaryDto[]> getSalaryInfo(@RequestBody CredentialDto credentialDto) {
        List<BpjsTkSalary> result = bpjsTkClient.getSalaryInfo(credentialDto.getEmail(), credentialDto.getPassword());
        if (result != null) {
            return ResponseEntity.ok(MODEL_MAPPER.map(result, BpjsTkSalaryDto[].class));
        }
        return ResponseEntity.unprocessableEntity().build();
    }
}
