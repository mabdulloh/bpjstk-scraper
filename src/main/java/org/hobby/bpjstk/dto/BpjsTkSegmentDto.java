package org.hobby.bpjstk.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BpjsTkSegmentDto {
    private String kodeTk;
    private String namaPerusahaan;
    private double nomSaldo;
    private String kodeSegmen;
    private String kpj;
    private String kodePerusahaan;
    private String npp;
}
