package org.hobby.bpjstk.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BpjsTkSalary {
    private String kodePerusahaan;
    private String blth;
    private String kodeSegmen;
    private String kodeKepesertaan;
    private String kodeTk;
    private String kodeIuran;
    private String tglPniPelunasan;
    private double nomIuranJht;
    private double nomUpah;
}
