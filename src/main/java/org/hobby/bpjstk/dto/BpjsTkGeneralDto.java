package org.hobby.bpjstk.dto;

import lombok.Getter;
import lombok.Setter;
import org.hobby.bpjstk.model.BpjsTkKpj;

import java.util.List;

@Getter
@Setter
public class BpjsTkGeneralDto {
    private String nik;
    private String namaLengkap;
    private String nomorHp;
    private String email;
    private String alamat;
    private String tglLahir;
    private String jenisKelamin;
    private List<BpjsTkKpj> kpj;
}
